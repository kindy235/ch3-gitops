{{/*
Expand the name of the chart.
*/}}
{{- define "AppCtx.chartName" -}}
{{- default .Chart.Name | trunc 24 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "AppCtx.chartNameVersion" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Application name
*/}}
{{- define "AppCtx.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s" $name | trunc 30 | trimSuffix "-"}}
{{- end }}

{{/*
API Name
*/}}
{{- define "AppCtx.api.name" }}
{{- printf "%s-api" (include "AppCtx.fullname" .) }}
{{- end }}

{{/*
Front Name
*/}}
{{- define "AppCtx.front.name" }}
{{- printf "%s-front" (include "AppCtx.fullname" .)  }}
{{- end }}

{{/*
database Name
*/}}
{{- define "AppCtx.database.name" }}
{{- printf "%s-database" (include "AppCtx.fullname" .) }}
{{- end }}
